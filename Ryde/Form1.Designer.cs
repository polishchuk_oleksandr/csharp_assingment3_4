﻿namespace Ryde
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.radioButtonPool = new System.Windows.Forms.RadioButton();
            this.radioButtonDirect = new System.Windows.Forms.RadioButton();
            this.buttonFindRide = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFrom.Location = new System.Drawing.Point(47, 172);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(68, 25);
            this.labelFrom.TabIndex = 0;
            this.labelFrom.Text = "From:";
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTo.Location = new System.Drawing.Point(47, 214);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(45, 25);
            this.labelTo.TabIndex = 1;
            this.labelTo.Text = "To:";
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFrom.Location = new System.Drawing.Point(135, 169);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(160, 30);
            this.textBoxFrom.TabIndex = 2;
            // 
            // textBoxTo
            // 
            this.textBoxTo.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.textBoxTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTo.Location = new System.Drawing.Point(135, 209);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(160, 30);
            this.textBoxTo.TabIndex = 3;
            // 
            // radioButtonPool
            // 
            this.radioButtonPool.AutoSize = true;
            this.radioButtonPool.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.radioButtonPool.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonPool.Location = new System.Drawing.Point(135, 255);
            this.radioButtonPool.Name = "radioButtonPool";
            this.radioButtonPool.Size = new System.Drawing.Size(116, 29);
            this.radioButtonPool.TabIndex = 4;
            this.radioButtonPool.TabStop = true;
            this.radioButtonPool.Text = "Ride Pool";
            this.radioButtonPool.UseVisualStyleBackColor = false;
            // 
            // radioButtonDirect
            // 
            this.radioButtonDirect.AutoSize = true;
            this.radioButtonDirect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDirect.Location = new System.Drawing.Point(135, 301);
            this.radioButtonDirect.Name = "radioButtonDirect";
            this.radioButtonDirect.Size = new System.Drawing.Size(127, 29);
            this.radioButtonDirect.TabIndex = 5;
            this.radioButtonDirect.TabStop = true;
            this.radioButtonDirect.Text = "Ride Direct";
            this.radioButtonDirect.UseVisualStyleBackColor = true;
            // 
            // buttonFindRide
            // 
            this.buttonFindRide.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonFindRide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFindRide.ForeColor = System.Drawing.SystemColors.Desktop;
            this.buttonFindRide.Location = new System.Drawing.Point(135, 348);
            this.buttonFindRide.Name = "buttonFindRide";
            this.buttonFindRide.Size = new System.Drawing.Size(160, 33);
            this.buttonFindRide.TabIndex = 6;
            this.buttonFindRide.Text = "FIND MY RIDE";
            this.buttonFindRide.UseVisualStyleBackColor = false;
            this.buttonFindRide.Click += new System.EventHandler(this.buttonFindRide_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(135, 58);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 67);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(334, 448);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonFindRide);
            this.Controls.Add(this.radioButtonDirect);
            this.Controls.Add(this.radioButtonPool);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.textBoxFrom);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.labelFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.RadioButton radioButtonPool;
        private System.Windows.Forms.RadioButton radioButtonDirect;
        private System.Windows.Forms.Button buttonFindRide;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

