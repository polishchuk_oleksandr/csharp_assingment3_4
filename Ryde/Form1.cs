﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ryde
{
    public partial class Form1 : Form
    {

        public string from, to;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonFindRide_Click(object sender, EventArgs e)
        {
            bool validation = true;
            from = textBoxFrom.Text;
            to = textBoxTo.Text;
            Locations[] array = new Locations[2];
            array[0] = new Locations("275 Yorkland Blvd", "CN Tower", 22.90);
            array[1] = new Locations("Fairview Mall", "Tim Hortons", 1.20);

            if (String.IsNullOrWhiteSpace(from) || String.IsNullOrWhiteSpace(to))
            {
                MessageBox.Show("From or to text box is empty! Check it out again.", "Validation");
                validation = false;
            }
            else if (!radioButtonDirect.Checked && !radioButtonPool.Checked)
            {
                MessageBox.Show("Check type ryde!", "Validation");
                validation = false;
            }

            
            if (validation)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    Locations c = array[i];
                    if (c != null)
                    {
                        if (c.ValidateTrip(from, to))
                        {
                            Form2 screen2 = new Form2(from, to, c.kilometers, radioButtonDirect.Checked);
                            validation = true;
                            screen2.ShowDialog();
                            this.Close();
                        }
                    }

                };
            }
            
            MessageBox.Show("Wrong trip combination! Try again...", "Validation");




        }
    }
}
