﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ryde
{
    public partial class Form2 : Form
    {
        public Form2(string from, string to, double km, bool type)
        {

            DateTime dt1 = Convert.ToDateTime(DateTime.Now);
            DateTime dt2 = Convert.ToDateTime("10:00");
            DateTime dt3 = Convert.ToDateTime("12:00");
            DateTime dt4 = Convert.ToDateTime("16:00");
            DateTime dt5 = Convert.ToDateTime("18:00");
            DateTime dt6 = Convert.ToDateTime("20:00");
            DateTime dt7 = Convert.ToDateTime("21:00");

            double baseFare = type ? 2.50 * 1.1 : 2.50; ;
            double distantChargeperKm = 0.81;

            if ((dt1.TimeOfDay.Ticks > dt2.TimeOfDay.Ticks && dt1.TimeOfDay.Ticks < dt3.TimeOfDay.Ticks)
                || (dt1.TimeOfDay.Ticks > dt4.TimeOfDay.Ticks && dt1.TimeOfDay.Ticks < dt5.TimeOfDay.Ticks)
                || (dt1.TimeOfDay.Ticks > dt6.TimeOfDay.Ticks && dt1.TimeOfDay.Ticks < dt7.TimeOfDay.Ticks))
            {
                distantChargeperKm = distantChargeperKm * 1.2;
                
            }

            double distantCharge = type ? Math.Round((distantChargeperKm * 1.15) * km, 2) : Math.Round(distantChargeperKm * km, 2);
            double serviceFee = 1.75;
            double minimumFare = 5.50;
            double total = baseFare + distantCharge + serviceFee;

            if (total < minimumFare) total = minimumFare;

            InitializeComponent();

            labelPeakHour.Text = distantChargeperKm > 0.81 ? "*Peak hours rate was applied": "";
            labelFromVal.Text = from;
            labelToVal.Text = to;
            labelBookingFeeVal.Text = "$" + baseFare.ToString();
            labelDistanceChargeVal.Text = "$" + distantCharge.ToString();
            labelServiceFeeVal.Text = "$" + serviceFee.ToString();
            labelTotalVal.Text = "$" + total.ToString();
            


        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
