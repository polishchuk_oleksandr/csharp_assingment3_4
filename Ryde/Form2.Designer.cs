﻿namespace Ryde
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConfirm = new System.Windows.Forms.Label();
            this.labelTypeRyde = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.labelBookingFee = new System.Windows.Forms.Label();
            this.labelDistanceCharge = new System.Windows.Forms.Label();
            this.labelServiceFee = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelFromVal = new System.Windows.Forms.Label();
            this.labelToVal = new System.Windows.Forms.Label();
            this.labelBookingFeeVal = new System.Windows.Forms.Label();
            this.labelDistanceChargeVal = new System.Windows.Forms.Label();
            this.labelServiceFeeVal = new System.Windows.Forms.Label();
            this.labelTotalVal = new System.Windows.Forms.Label();
            this.labelPeakHour = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelConfirm
            // 
            this.labelConfirm.AutoSize = true;
            this.labelConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConfirm.Location = new System.Drawing.Point(144, 61);
            this.labelConfirm.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelConfirm.Name = "labelConfirm";
            this.labelConfirm.Size = new System.Drawing.Size(108, 25);
            this.labelConfirm.TabIndex = 0;
            this.labelConfirm.Text = "Confirmed!";
            // 
            // labelTypeRyde
            // 
            this.labelTypeRyde.AutoSize = true;
            this.labelTypeRyde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTypeRyde.Location = new System.Drawing.Point(28, 61);
            this.labelTypeRyde.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelTypeRyde.Name = "labelTypeRyde";
            this.labelTypeRyde.Size = new System.Drawing.Size(110, 25);
            this.labelTypeRyde.TabIndex = 1;
            this.labelTypeRyde.Text = "Ryde Pool";
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFrom.Location = new System.Drawing.Point(30, 124);
            this.labelFrom.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(48, 18);
            this.labelFrom.TabIndex = 2;
            this.labelFrom.Text = "From:";
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTo.Location = new System.Drawing.Point(30, 156);
            this.labelTo.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(30, 18);
            this.labelTo.TabIndex = 3;
            this.labelTo.Text = "To:";
            // 
            // labelBookingFee
            // 
            this.labelBookingFee.AutoSize = true;
            this.labelBookingFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookingFee.Location = new System.Drawing.Point(30, 214);
            this.labelBookingFee.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelBookingFee.Name = "labelBookingFee";
            this.labelBookingFee.Size = new System.Drawing.Size(91, 18);
            this.labelBookingFee.TabIndex = 4;
            this.labelBookingFee.Text = "Booking fee:";
            // 
            // labelDistanceCharge
            // 
            this.labelDistanceCharge.AutoSize = true;
            this.labelDistanceCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDistanceCharge.Location = new System.Drawing.Point(30, 245);
            this.labelDistanceCharge.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelDistanceCharge.Name = "labelDistanceCharge";
            this.labelDistanceCharge.Size = new System.Drawing.Size(119, 18);
            this.labelDistanceCharge.TabIndex = 5;
            this.labelDistanceCharge.Text = "Distance charge:";
            // 
            // labelServiceFee
            // 
            this.labelServiceFee.AutoSize = true;
            this.labelServiceFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServiceFee.Location = new System.Drawing.Point(30, 279);
            this.labelServiceFee.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelServiceFee.Name = "labelServiceFee";
            this.labelServiceFee.Size = new System.Drawing.Size(85, 18);
            this.labelServiceFee.TabIndex = 6;
            this.labelServiceFee.Text = "Service fee:";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotal.Location = new System.Drawing.Point(30, 345);
            this.labelTotal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(51, 18);
            this.labelTotal.TabIndex = 7;
            this.labelTotal.Text = "Total:";
            // 
            // labelFromVal
            // 
            this.labelFromVal.AutoSize = true;
            this.labelFromVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFromVal.Location = new System.Drawing.Point(80, 124);
            this.labelFromVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelFromVal.Name = "labelFromVal";
            this.labelFromVal.Size = new System.Drawing.Size(46, 18);
            this.labelFromVal.TabIndex = 8;
            this.labelFromVal.Text = "label7";
            // 
            // labelToVal
            // 
            this.labelToVal.AutoSize = true;
            this.labelToVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToVal.Location = new System.Drawing.Point(80, 156);
            this.labelToVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelToVal.Name = "labelToVal";
            this.labelToVal.Size = new System.Drawing.Size(46, 18);
            this.labelToVal.TabIndex = 9;
            this.labelToVal.Text = "label8";
            // 
            // labelBookingFeeVal
            // 
            this.labelBookingFeeVal.AutoSize = true;
            this.labelBookingFeeVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookingFeeVal.Location = new System.Drawing.Point(157, 214);
            this.labelBookingFeeVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelBookingFeeVal.Name = "labelBookingFeeVal";
            this.labelBookingFeeVal.Size = new System.Drawing.Size(46, 18);
            this.labelBookingFeeVal.TabIndex = 10;
            this.labelBookingFeeVal.Text = "label9";
            // 
            // labelDistanceChargeVal
            // 
            this.labelDistanceChargeVal.AutoSize = true;
            this.labelDistanceChargeVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDistanceChargeVal.Location = new System.Drawing.Point(157, 245);
            this.labelDistanceChargeVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelDistanceChargeVal.Name = "labelDistanceChargeVal";
            this.labelDistanceChargeVal.Size = new System.Drawing.Size(54, 18);
            this.labelDistanceChargeVal.TabIndex = 11;
            this.labelDistanceChargeVal.Text = "label10";
            // 
            // labelServiceFeeVal
            // 
            this.labelServiceFeeVal.AutoSize = true;
            this.labelServiceFeeVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServiceFeeVal.Location = new System.Drawing.Point(157, 279);
            this.labelServiceFeeVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelServiceFeeVal.Name = "labelServiceFeeVal";
            this.labelServiceFeeVal.Size = new System.Drawing.Size(54, 18);
            this.labelServiceFeeVal.TabIndex = 12;
            this.labelServiceFeeVal.Text = "label11";
            // 
            // labelTotalVal
            // 
            this.labelTotalVal.AutoSize = true;
            this.labelTotalVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalVal.Location = new System.Drawing.Point(120, 345);
            this.labelTotalVal.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelTotalVal.Name = "labelTotalVal";
            this.labelTotalVal.Size = new System.Drawing.Size(61, 18);
            this.labelTotalVal.TabIndex = 13;
            this.labelTotalVal.Text = "label12";
            // 
            // labelPeakHour
            // 
            this.labelPeakHour.AutoSize = true;
            this.labelPeakHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeakHour.Location = new System.Drawing.Point(30, 313);
            this.labelPeakHour.Margin = new System.Windows.Forms.Padding(3, 0, 6, 0);
            this.labelPeakHour.Name = "labelPeakHour";
            this.labelPeakHour.Size = new System.Drawing.Size(54, 18);
            this.labelPeakHour.TabIndex = 14;
            this.labelPeakHour.Text = "label11";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 450);
            this.Controls.Add(this.labelPeakHour);
            this.Controls.Add(this.labelTotalVal);
            this.Controls.Add(this.labelServiceFeeVal);
            this.Controls.Add(this.labelDistanceChargeVal);
            this.Controls.Add(this.labelBookingFeeVal);
            this.Controls.Add(this.labelToVal);
            this.Controls.Add(this.labelFromVal);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.labelServiceFee);
            this.Controls.Add(this.labelDistanceCharge);
            this.Controls.Add(this.labelBookingFee);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.labelFrom);
            this.Controls.Add(this.labelTypeRyde);
            this.Controls.Add(this.labelConfirm);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConfirm;
        private System.Windows.Forms.Label labelTypeRyde;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label labelBookingFee;
        private System.Windows.Forms.Label labelDistanceCharge;
        private System.Windows.Forms.Label labelServiceFee;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelFromVal;
        private System.Windows.Forms.Label labelToVal;
        private System.Windows.Forms.Label labelBookingFeeVal;
        private System.Windows.Forms.Label labelDistanceChargeVal;
        private System.Windows.Forms.Label labelServiceFeeVal;
        private System.Windows.Forms.Label labelTotalVal;
        private System.Windows.Forms.Label labelPeakHour;
    }
}