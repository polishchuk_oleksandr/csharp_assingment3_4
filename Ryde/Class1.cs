﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryde
{
    class Locations
    {
        public string from, to;
        public double kilometers;

        public Locations(string fr, string t, double km) {
            this.from = fr;
            this.to = t;
            this.kilometers = km;
        }

        public bool ValidateTrip(string fr, string t)
        {
            if (this.from.Equals(fr) && this.to.Equals(t))
            {
                return true;
            }
            else {
                return false;
            }
        }
    }

    
}
